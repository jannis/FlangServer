# FlangServer

This is the server for the [Flang board game](https://codeberg.org/jannis/Flang) coded in [Groovy](https://groovy-lang.org/) using [Grails](https://grails.org/).

It stores all the user data and provides a platform for multiplayer games.

## Features

- User authentication
- Game requests
  - see other requests
  - accept them
- Games
  - make moves
  - resign
  - play against bots
- Flang TV
- See user profiles

## Setup

Install `git` and `gradle`.

Clone the repository using git:

```
git clone https://codeberg.org/jannis/FlangServer
```

### for Development

Install [Grails](https://grails.org/).

Open the project with an IDE (Idea is recommended). 
If you have the Idea Ultimate version, you are lucky and will have native Grails support. Then it'll configure everything automatically.
Otherwise, you can start the application with this command: `grails run-app`.

Before it runs, you need a development database.

Install mysql. On Ubuntu/Mint:

```
apt install mysql-server
```

Create a development database on your mysql server and put the credentials into the file `grails-app/conf/application.yml`
down at:

```
dataSource:
    username: flang
    password: secret_flang_password
```

Also don't forget to specify where your database runs. Change `flang_dev` to whatever you called the database.

```
environments:
    development:
        dataSource:
            dbCreate: update
            url: jdbc:mysql://localhost/flang_dev
```

Now try to run :)

### for Production

On your server you need to install the tomcat webserver and a mysql database. On modern versions of Ubuntu this can be done by:

```
apt install tomcat9 mysql-server
```

Open the file `grails-app/conf/application.yml` using your favorite editor.

Please enter the new credentials for your database:

```
dataSource:
    username: flang
    password: secret_flang_password
```

Also don't forget to specify where your database runs. Change `localhost` to the host and `flang` to whatever you called the database.

```
environments:
    production:
        dataSource:
            dbCreate: update
            url: jdbc:mysql://localhost/flang?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC
```

then execute

```
gradle war
```

This will generate a war file under `build/libs/`. This war file can be run on your webserver.

Put your war file into the `webapps` directory.
If you installed the tomcat webserver using `apt`, that should be located here: `/var/lib/tomcat9/webapps/`.

The name of your war-file determines where the flang server will be available.
If you call it `flang.war`, it'll be available under `/flang`.
`ROOT.war` means, it will be available as the webserver root `/`.

To test if it deployed correctly you can type `http://host:port/name` (e.g. `http://localhost:8080/flang`) in your browser.

## License

Copyright (C) 2021 Jannis Scheibe <jannis@tadris.de>

	Flang is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Flang is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Flang ist Freie Software: Sie können es unter den Bedingungen
    der GNU General Public License, wie von der Free Software Foundation,
    Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
    veröffentlichten Version, weiter verteilen und/oder modifizieren.

    Flang wird in der Hoffnung bereitgestellt, dass es nützlich sein wird, jedoch
    OHNE JEDE GEWÄHR,; sogar ohne die implizite
    Gewähr der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
    Siehe die GNU General Public License für weitere Einzelheiten.

    Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
    Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.