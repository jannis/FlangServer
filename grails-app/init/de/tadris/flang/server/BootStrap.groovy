package de.tadris.flang.server

import de.tadris.flang.server.main.FlangServer

class BootStrap {

    def init = { servletContext ->
        new FlangServer()
    }
    def destroy = {
        FlangServer.instance.destroy()
    }
}
