package de.tadris.flang.server

import de.tadris.flang.server.main.Log
import grails.boot.GrailsApp
import grails.boot.config.GrailsAutoConfiguration
import grails.util.Environment
import groovy.transform.CompileStatic

@CompileStatic
class Application extends GrailsAutoConfiguration {
    static void main(String[] args) {
        if(Environment.current == Environment.DEVELOPMENT){
            println "Mode: verbose"
            Log.MIN_LOGLEVEL= Log.DEBUG
        }

        if(args.size()>0 && args[0] == "-v"){
            println "Mode: verbose"
            println "Loglevel: INFO"
            println "For even more output use -vv"
            Log.MIN_LOGLEVEL= Log.INFO
        }
        if(args.size()>0 && args[0] == "-vv"){
            println "Mode: super-verbose"
            println "Loglevel: DEBUG"
            Log.MIN_LOGLEVEL= Log.DEBUG
        }

        GrailsApp.run(Application, args)
    }
}