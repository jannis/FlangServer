package de.tadris.flang.server

class ComputerResult {

    static constraints = {
        result(size: 0..2048)
    }

    String fbn
    boolean isWhite
    int depth
    String result
    String algorithm
    int usedCount = 0
    Date createdAt

}
