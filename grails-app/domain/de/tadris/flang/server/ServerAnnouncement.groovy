package de.tadris.flang.server

class ServerAnnouncement {

    static constraints = {
    }

    Date showUntil
    String title
    String text
    String url

}
