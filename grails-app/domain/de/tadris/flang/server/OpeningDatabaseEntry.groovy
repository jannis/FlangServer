package de.tadris.flang.server

class OpeningDatabaseEntry {

    static constraints = {
    }

    String fbn
    boolean isWhite
    String move
    int gameCount = 0
    int winCount = 0
    int looseCount = 0

}
