package de.tadris.flang.server

import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Color
import de.tadris.flang_lib.action.Action
import de.tadris.flang_lib.action.ActionList

import java.util.concurrent.TimeUnit

class Game {

    static final long INFINITE_GAME_TIMEOUT = TimeUnit.DAYS.toMillis(7) // Time after which infinite games are closed

    static final int WON_BY_FLANG = 1 // When the opponent is mate
    static final int WON_BY_BASE = 2  // When king is in the opponents base

    static final int WON_BY_RESIGN = 4
    static final int WON_BY_TIMEOUT = 8

    static constraints = {
        fmn(size: 0..3000)
    }

    static belongsTo = [white: User, black: User]

    boolean isActive
    String fmn = "" // Flang move notation - contains all moves to reproduce the game
    int moveCount = 0
    Date lastMove = new Date()
    long startTime
    long timeLeftWhite
    long timeLeftBlack
    boolean infiniteTime
    boolean isRated
    int won // Positive -> white, negative -> black
    float whiteRating // can be negative!
    float blackRating // can be negative!
    float whiteRatingDiff = 0
    float blackRatingDiff = 0

    Board toBoard(){
        if(fmn == ""){ return new Board(Board.DEFAULT_BOARD, Color.WHITE) }
        return new ActionList().addMoveList(fmn).board
    }

    void appendAction(Action action, Color color){
        if(!fmn.empty) fmn+= " "
        fmn+= action.toString()
        if(toBoard().gameIsComplete()){
            isActive = false
        }
        if(!infiniteTime){
            if(color == Color.WHITE){
                timeLeftWhite-= timeDiff()
                lastMove = new Date()
            }else{
                timeLeftBlack-= timeDiff()
                lastMove = new Date()
            }
        }
    }

    long timeDiff(){
        return System.currentTimeMillis() - lastMove.time
    }

    void applyBoard(Board board){
        this.fmn = board.getFMN()
        this.moveCount = board.moveNumber
        this.isActive = !board.gameIsComplete()
    }

    long timeLeftFor(Color color){
        long timeLeft = color == Color.WHITE ? timeLeftWhite : timeLeftBlack
        if(isActive && toBoard().atMove == color){
            timeLeft-= timeDiff()
        }
        return timeLeft
    }

    boolean gameTimeout(){
        if(infiniteTime){
            return System.currentTimeMillis() - lastMove.time > INFINITE_GAME_TIMEOUT
        }else{
            Color atMove = toBoard().atMove
            return timeLeftFor(atMove) < 0
        }
    }

    Map toMap(){
        return [
                "gameId": id,
                "white": [
                        "username": white.username,
                        "rating": whiteRating,
                        "ratingDiff": whiteRatingDiff,
                        "time": timeLeftFor(Color.WHITE),
                        "isBot": white.isBot,
                        "title": white.title,
                ],
                "black": [
                        "username": black.username,
                        "rating": blackRating,
                        "ratingDiff": blackRatingDiff,
                        "time": timeLeftFor(Color.BLACK),
                        "isBot": black.isBot,
                        "title": black.title,
                ],
                "fmn": fmn,
                "moves": moveCount,
                "running": isActive,
                "configuration": [
                        "infiniteTime": infiniteTime,
                        "time": startTime,
                        "isRated": isRated
                ],
                "lastAction": lastMove.time,
                "won": won,
                "spectatorCount": SpectatingUsers.getSpectatorCount(id),
        ]
    }

}
