package de.tadris.flang.server

class Message {

    static constraints = {
        text(size: 0..1024)
        attachedGame(size: 0..2000)
    }

    String sender
    String senderTitle

    Date sentAt
    String text
    String attachedGame

    Map toMap(){
        return [
                "sender": [username: sender, title: senderTitle, rating: -1f, isBot: false],
                "date": sentAt.time,
                "text": text,
                "game": gameToMap()
        ]
    }

    private Map gameToMap(){
        if(attachedGame.empty){
            return null
        }else{
            long gameId = -1
            String fmn = ""
            String fbn = ""
            if(attachedGame.isLong()){
                gameId = attachedGame.toLong()
                Game game = Game.findById(gameId)
                if(game != null){
                    fmn = game.fmn
                    fbn = game.toBoard().getFBN()
                }
            }else if(attachedGame.startsWith("+") || attachedGame.startsWith("-")){
                fbn = attachedGame
            }else{
                fmn = attachedGame
            }
            return [
                    "id": gameId.toString(),
                    "fmn": fmn,
                    "fbn": fbn
            ]
        }
    }

}
