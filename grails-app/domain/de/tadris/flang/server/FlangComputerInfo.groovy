package de.tadris.flang.server

class FlangComputerInfo {

    static constraints = {
    }

    String username
    String loginKey
    String host
    int depth
    int estimatedRating
    int port
    String algorithm

}
