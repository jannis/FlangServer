package de.tadris.flang.server

class User {

    static final int STANDARD_RATING = 1300

    static constraints = {
    }

    static hasMany = [ratingEntries: UserRatingEntry]

    String username
    String pwdHash

    float rating
    boolean isBot
    boolean isChatBanned = false
    int completedGames = 0
    String title = ""

    Date registeredAt

    float getNewRating(float opponentRating, boolean hasWon){
        int gameResult = hasWon ? 1 : 0
        double expected = 1 / (1 + Math.pow(10, (opponentRating - rating) / 400))
        float diff = weight*(gameResult - expected) + RatingBonus.getCurrentBonus(hasWon) as float

        // winner can only get a positive difference and the looser only negative
        // this could be wrong because of the bonus
        if((hasWon && diff > 0) || (!hasWon && diff < 0)){
            return rating + diff
        }else{
            return rating
        }
    }

    float getDisplayedRating(){
        return rating * (completedGames >= 15 ? 1 : -1)
    }

    private int getWeight(){
        if(completedGames < 5){
            return 160
        }else if(completedGames < 15){
            return 80
        }else if(completedGames < 30){
            return 40
        }else if(rating > 2400){
            return 10
        }else{
            return 20
        }
    }

}
