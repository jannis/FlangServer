package de.tadris.flang.server

class UserSession {

    static constraints = {
    }

    static belongsTo = [user: User]

    String sessionKey
    Date lastLogin
    boolean temporary = false

}
