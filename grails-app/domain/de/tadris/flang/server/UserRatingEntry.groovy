package de.tadris.flang.server

class UserRatingEntry {

    static constraints = {
    }

    static belongsTo = [user: User]

    Date createdAt
    float rating

    Map toMap(){
        return [date: createdAt.time, rating: rating]
    }

}
