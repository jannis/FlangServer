package de.tadris.flang.server

class DayStats {

    static constraints = { }

    Date date
    int avgRating
    int activePlayersLastDay
    int gamesLastDay

}
