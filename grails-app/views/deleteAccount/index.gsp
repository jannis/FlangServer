<!doctype html>
<html>
<head>
    <title>Delete your Flang Account</title>
</head>
<body>

<h1>Delete your Flang Account</h1>
<h2>How it works</h2>

<ol>
    <li>Use the form below and enter username and password.</li>
    <li>Click on submit to delete your account.</li>
    <li>The deletion will instantly delete all data associated with the account including login and game data.</li>
    <li><b>The deletion is permanent and cannot be taken back.</b></li>
</ol>

<h2>Deletion form</h2>

<form action="submitDeletion" name="deletionForm" method="post" class="form">
    <fieldset>
        <div class="field">
            <label for="username">Username</label>
            <input id="username" name="username"/>
        </div>
        <div class="field">
            <label for="password">Password</label>
            <input id="password" name="password"/>
        </div>
    </fieldset>
    <g:submitButton class="submit" name="submit" value="Delete"/>
</form>

</body>
</html>
