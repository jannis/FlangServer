package de.tadris.flang.server

import grails.gorm.transactions.Transactional

@Transactional
class DeleteAccountService {

    private static final int ANONYMOUS_ACCOUNT_ID = 466

    def deleteAccount(User user) {
        User anonymous = User.findById(ANONYMOUS_ACCOUNT_ID)
        Game.findAllByWhite(user).each { game ->
            game.white = anonymous
            game.save()
        }
        Game.findAllByBlack(user).each { game ->
            game.black = anonymous
            game.save()
        }
        UserRatingEntry.findAllByUser(user).each {
            it.delete()
        }
        UserSession.findAllByUser(user).each {
            it.delete()
        }

        user.delete()
    }
}
