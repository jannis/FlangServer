package de.tadris.flang.server

import de.tadris.flang.server.game.GameConfiguration
import de.tadris.flang.server.game.GameUpdates
import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Color
import de.tadris.flang_lib.action.Action
import de.tadris.flang_lib.action.Resign
import grails.gorm.transactions.Transactional

@Transactional
class GameService {

    def userRatingService

    Game createNewGame(GameConfiguration configuration, long user1, long user2) {
        return createNewGame(configuration, User.findById(user1), User.findById(user2))
    }

    Game createNewGame(GameConfiguration configuration, User user1, User user2) {
        boolean user1IsWhite = Math.random() > 0.5

        Game game = new Game()
        game.white = user1IsWhite ? user1 : user2
        game.black = user1IsWhite ? user2 : user1
        game.isActive = true
        game.infiniteTime = configuration.infiniteTime
        game.startTime = configuration.time
        game.timeLeftWhite = game.startTime
        game.timeLeftBlack = game.startTime
        game.whiteRating = game.white.displayedRating
        game.blackRating = game.black.displayedRating
        game.isRated = configuration.isRated
        game.applyBoard(new Board(Board.DEFAULT_BOARD, Color.WHITE))
        game.insert()

        return game
    }

    boolean checkForTimeout(Game game){
        if(game.gameTimeout()){
            Color timeoutColor = game.toBoard().atMove
            executeAction(game, new Resign(timeoutColor), timeoutColor, true)
            return true
        }else{
            return false
        }
    }

    void executeAction(Game game, Action action, Color color, boolean isTimeout = false){
        game.appendAction(action, color)
        game.moveCount++
        game.save()
        if(!game.isActive){
            onGameFinish(game, isTimeout, action instanceof Resign)
        }
        GameUpdates.onUpdate(game)
    }

    void onGameFinish(Game game, boolean isTimeout, boolean isResign){
        Board board = game.toBoard()
        game.isActive = false
        User white = game.white
        User black = game.black
        float lastRatingWhite = white.rating
        float lastRatingBlack = black.rating

        Color winningColor = board.hasWon(Color.WHITE) ? Color.WHITE : Color.BLACK

        int reason
        if(isTimeout){
            reason = Game.WON_BY_TIMEOUT
        }else if(isResign){
            reason = Game.WON_BY_RESIGN
        }else if(board.findKing(winningColor).location.y == winningColor.winningY){
            reason = Game.WON_BY_BASE
        }else{
            reason = Game.WON_BY_FLANG
        }

        game.won = winningColor.evaluationNumber * reason

        if(game.isRated){
            white.rating = white.getNewRating(lastRatingBlack, board.hasWon(Color.WHITE))
            game.whiteRatingDiff = white.rating - game.whiteRating.abs() as float
            black.rating = black.getNewRating(lastRatingWhite, board.hasWon(Color.BLACK))
            game.blackRatingDiff = black.rating - game.blackRating.abs() as float
        }

        white.completedGames++
        black.completedGames++

        userRatingService?.update(white)
        userRatingService?.update(black)

        white.save()
        black.save()
        game.save()

        // Add game to opening database
        OpeningDatabaseBuilder.addGame(game)
    }
}
