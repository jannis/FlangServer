package de.tadris.flang.server

import de.tadris.flang.server.chat.ChatUpdates
import grails.gorm.transactions.Transactional

@Transactional
class ChatService {

    def sendMessage(User sender, String text, String attachedGame) {
        Message message = new Message()
        message.sender = sender.username
        message.senderTitle = sender.title
        message.attachedGame = attachedGame
        message.sentAt = new Date()
        message.text = text
        message.save()
        ChatUpdates.onUpdate(message)
    }
}
