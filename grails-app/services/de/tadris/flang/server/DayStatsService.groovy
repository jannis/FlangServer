package de.tadris.flang.server

import grails.gorm.transactions.Transactional

import java.util.concurrent.TimeUnit

@Transactional
class DayStatsService {

    void collectStats() {
        Date dayAgo = new Date(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(1))

        DayStats dayStats = new DayStats()
        dayStats.date = new Date()
        dayStats.avgRating = ((User.findAll()*.rating.sum() as float) / User.count()).toInteger()
        dayStats.activePlayersLastDay = UserSession
                .findAllByLastLoginGreaterThan(dayAgo)
                .collect { it.userId }
                .unique()
                .size()
        dayStats.gamesLastDay = Game.countByLastMoveGreaterThan(dayAgo)
        dayStats.insert()
    }

}
