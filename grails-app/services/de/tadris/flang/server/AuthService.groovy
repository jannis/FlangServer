package de.tadris.flang.server

import grails.gorm.transactions.Transactional

@Transactional
class AuthService {

    def createUser(User user) {
        user.registeredAt = new Date()
        user.save()
    }

    UserSession createSession(User user, boolean tempSession = false){
        UserSession session = new UserSession()
        session.user = user
        session.lastLogin = new Date()
        session.sessionKey = UUID.randomUUID().toString().sha256()
        session.temporary = tempSession
        session.save()
        return session
    }

    def onActive(UserSession session){
        session.lastLogin = new Date()
        session.save()
    }

}
