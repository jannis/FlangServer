package de.tadris.flang.server

import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Color
import de.tadris.flang_lib.action.ActionList
import grails.gorm.transactions.Transactional

@Transactional
class OpeningDatabaseService {

    List<OpeningDatabaseEntry> query(String fmn) {
        Board board = new ActionList().addMoveList(fmn).board
        def results = OpeningDatabaseEntry.findAllByFbnAndIsWhite(board.FBN, board.atMove == Color.WHITE).sort { -it.gameCount }
        return results
    }
}
