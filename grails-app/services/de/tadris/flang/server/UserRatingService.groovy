package de.tadris.flang.server

import grails.gorm.transactions.Transactional

@Transactional
class UserRatingService {

    def update(User user) {
        clearRecent(user)
        UserRatingEntry entry = new UserRatingEntry()
        entry.user = user
        entry.createdAt = new Date()
        entry.rating = user.rating
        entry.insert()
    }

    def clearRecent(User user){
        Calendar calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        long startOfDay = calendar.timeInMillis
        UserRatingEntry.findAllByUserAndCreatedAtGreaterThan(user, new Date(startOfDay)).each {
            it.delete()
        }
    }

}
