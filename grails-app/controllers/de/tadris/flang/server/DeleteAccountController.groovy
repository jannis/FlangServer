package de.tadris.flang.server

import de.tadris.flang.server.main.BruteforceProtection
import grails.converters.JSON

import java.util.regex.Pattern

import static org.springframework.http.HttpStatus.*

class DeleteAccountController {

    def deleteAccountService

    def index(){ }

    synchronized def submitDeletion(String username, String password){
        sleep 1000

        if(username == null || password == null){
            render(status: BAD_REQUEST)
            return
        }
        if(BruteforceProtection.isTimeouted(request.remoteHost)){
            render (status: FORBIDDEN, text: "Your IP-address has been banned for too many requests.")
            return
        }

        if(!Pattern.matches("[A-Za-z0-9_-]{5,15}\$", username)){
            BruteforceProtection.countBruteforce(request.remoteHost)
            sleep(5000)
            render (status: FORBIDDEN, text: [
                    "code": 409,
                    "message": "Username is invalid"
            ] as JSON)
            return
        }

        String passwordHash = password.sha256()

        User user = User.findByUsername(username)
        if(user != null && passwordHash == user.pwdHash){
            deleteAccountService.deleteAccount(user)
            render (status: OK, text: "Your account was deleted.")
        }else{
            BruteforceProtection.countBruteforce(request.remoteHost)
            sleep 5000
            render (status: FORBIDDEN, text: "Wrong credentials.")
        }
    }

}
