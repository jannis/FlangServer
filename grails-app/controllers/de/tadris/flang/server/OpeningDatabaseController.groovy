package de.tadris.flang.server

import grails.converters.JSON

import static org.springframework.http.HttpStatus.OK

class OpeningDatabaseController {

    def openingDatabaseService

    def queryOpeningDatabase(String fmn) {
        try{
            def result = openingDatabaseService.query(fmn)
            return render (status: OK, text: [
                    "result": result.collect {[
                            "move": it.move,
                            "gameCount": it.gameCount,
                            "winCount": it.winCount,
                            "looseCount": it.looseCount,
                    ]},
            ] as JSON)
        }catch(Exception e){
            e.printStackTrace()
            return render (status: OK, text: [
                    "result": [],
            ] as JSON)
        }
    }
}
