package de.tadris.flang.server

import org.springframework.http.HttpStatus


class LoggedInterceptor {

    LoggedInterceptor() {
        match(controller: 'game')
        match(controller: 'chat')
    }

    boolean before() {
        if(actionName == "getLobby" || actionName == "getGameInfo"){
            // Override open methods in the game controller
            return true
        }
        if(!session.logged){
            render(status: HttpStatus.UNAUTHORIZED)
            return false
        }else{
            OnlineUsers.noteAsActive(session.userId as long)
            return true
        }
    }

    boolean after() { true }

    void afterView() {
        // no-op
    }

}
