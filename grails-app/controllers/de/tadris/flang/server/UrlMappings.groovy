package de.tadris.flang.server

class UrlMappings {

    static mappings = {
        "/"(view:"/index")
        //"500"(view:'/error')
        //"404"(view:'/notFound')

        "/info"(controller: 'global', action: 'info')
        "/register"(controller: 'auth', action: 'register')
        "/newSession"(controller: 'auth', action: 'newSession')
        "/login"(controller: 'auth', action: 'login')
        "/game/$id"(controller: 'game', action: 'getGameInfo')
        "/game/move/$id"(controller: 'game', action: 'executeMove')
        "/game/resign/$id"(controller: 'game', action: 'resign')
        "/game/request/lobby"(controller: 'game', action: 'getLobby')
        "/game/request/add"(controller: 'game', action: 'createGameRequest')
        "/game/request/accept/$id"(controller: 'game', action: 'acceptRequest')
        "/game/findActive"(controller: 'game', action: 'findActiveGame')
        "/users/top"(controller: 'user', action: 'getTopPlayers')
        "/users/online"(controller: 'user', action: 'getOnlinePlayers')
        "/user/$username"(controller: 'user', action: 'getInfo')
        "/user/$username/games"(controller: 'user', action: 'getGames')
        "/search/$username"(controller: 'user', action: 'search')
        "/tv"(controller: 'tv', action: 'index')
        "/computer/results"(controller: 'game', action: 'findComputerResults')
        "/opening/query"(controller: 'openingDatabase', action: 'queryOpeningDatabase')
        "/stats"(controller: 'stats', action: 'getStats')
        "/chat/global/messages"(controller: 'chat', action: 'getMessages')
        "/chat/global/send"(controller: 'chat', action: 'sendMessage')
        "/account/delete"(controller: 'deleteAccount', action: 'index')
        "/account/submitDeletion"(controller: 'deleteAccount', action: 'submitDeletion')
    }
}
