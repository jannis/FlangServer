package de.tadris.flang.server

import grails.converters.JSON
import org.springframework.http.HttpStatus

import java.util.regex.Pattern

class AuthController {

    def authService

    def register(String username, String pwdHash) {
        sleep 3000
        if(!Pattern.matches("[A-Za-z0-9_-]{5,15}\$", username)){
            sleep 3000
            render status: HttpStatus.BAD_REQUEST
            return
        }
        if(User.findByUsername(username) != null){
            sleep 3000
            render status: HttpStatus.FORBIDDEN
            return
        }

        User user = new User()
        user.username = username
        user.pwdHash = pwdHash
        user.rating = User.STANDARD_RATING

        authService.createUser(user)

        render status: HttpStatus.NO_CONTENT
    }

    def newSession(String username, String pwdHash){
        sleep 3000
        User user = User.findByUsernameAndPwdHash(username, pwdHash)
        if(user == null){
            sleep 3000
            render status: HttpStatus.FORBIDDEN
            return
        }
        UserSession session = authService.createSession(user)
        this.session.logged = true
        this.session.userId = user.id
        this.session.sessionId = session.id
        render status: HttpStatus.OK, text: (["key" : session.sessionKey] as JSON)
    }

    def login(String username, String key){
        sleep 500
        User user = User.findByUsername(username)
        if(user == null){
            sleep 3000
            render status: HttpStatus.FORBIDDEN
            return
        }
        UserSession session = UserSession.findByUserAndSessionKey(user, key)
        if(session == null){
            sleep 3000
            render status: HttpStatus.FORBIDDEN
        }else{
            authService.onActive(session)
            this.session.logged = true
            this.session.userId = user.id
            this.session.sessionId = session.id
            render status: HttpStatus.NO_CONTENT
        }
    }

}
