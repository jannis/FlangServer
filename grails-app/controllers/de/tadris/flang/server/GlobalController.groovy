package de.tadris.flang.server

import de.tadris.flang.server.main.FlangServer
import grails.converters.JSON

import static org.springframework.http.HttpStatus.OK

class GlobalController {

    def info(){
        render (status: OK, text: [
                "playerCount": (FlangServer.instance.metrics.onlineUsers.get() as int),
                "gameCount": (FlangServer.instance.metrics.activeGames.labels("active").get() as int),
                "announcements": ServerAnnouncement.findAll()
        ] as JSON)
    }
}
