package de.tadris.flang.server

import io.prometheus.client.Counter


class MetricInterceptor {

    final Counter requests = Counter.build("flang_request_count", "HTTP-Requests").register()

    MetricInterceptor() {
        matchAll()
    }

    boolean before() {
        requests.inc()
        return true
    }

    boolean after() { true }

    void afterView() {
        // no-op
    }

}
