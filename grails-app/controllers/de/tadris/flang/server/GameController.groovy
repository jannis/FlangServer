package de.tadris.flang.server

import de.tadris.flang.server.bot.FlangComputer
import de.tadris.flang.server.game.GameConfiguration
import de.tadris.flang.server.game.GameRequest
import de.tadris.flang.server.game.GameUpdates
import de.tadris.flang.server.main.Log
import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Color
import de.tadris.flang_lib.action.ActionList
import de.tadris.flang_lib.action.Move
import de.tadris.flang_lib.action.Resign
import grails.converters.JSON
import io.prometheus.client.Summary

import static org.springframework.http.HttpStatus.*

class GameController {

    static final int BOT_REQUEST_MAX_RATING_DIFF = 300

    def gameService

    def createGameRequest(boolean allowBots, long timeout, boolean isRated, boolean infiniteTime, long time, int range){
        timeout = Math.min(30000, Math.max(5000, timeout))
        User user = User.findById(session.userId as long)
        if(range == 0){
            range = -1
        }

        GameRequest request

        GameConfiguration configuration = new GameConfiguration(isRated, infiniteTime, time)

        if(allowBots){
            // If this request is for bots,
            request = new GameRequest(configuration, -1, user)
            def bots = FlangComputerInfo.findAllByEstimatedRatingBetween(
                    user.rating.toInteger()-BOT_REQUEST_MAX_RATING_DIFF,
                    user.rating.toInteger()+BOT_REQUEST_MAX_RATING_DIFF)
            if(bots.empty){
                bots = FlangComputerInfo.findAll()
            }

            FlangComputerInfo info = bots.get(Math.random()*bots.size() as int)
            Game game = gameService.createNewGame(configuration, user.id, User.findByUsername(info.username).id)
            FlangComputer computer = new FlangComputer(info)
            computer.requestGame(game)
            request.accept(game)
        }else{
            request = GameRequest.findAndAcceptRequest({ GameRequest req ->
                return configuration == req.configuration && req.requesterUserId != user.id && req.isInRange(user)
            }, { GameRequest req ->
                gameService.createNewGame(configuration, user.id, req.requesterUserId)
            })
        }

        if(request == null){
            // Make own request
            request = new GameRequest(configuration, range, user)
            GameRequest.insertRequest(request)
            Log.d("GameController", "$user.username - Request inserted")

            final Object lock = new Object()
            def listener = new GameRequest.RequestListener(){
                @Override
                void onUpdate(GameRequest req) {
                    if(req == request){
                        synchronized (lock){
                            lock.notifyAll()
                        }
                    }
                }
            }
            GameRequest.register(listener)
            try{
                synchronized (lock){
                    lock.wait(timeout)
                }
            }catch(InterruptedException ignored){
            }finally{
                GameRequest.unregister(listener)
            }

            GameRequest.removeRequest(request)
            Log.d("GameController", "$user.username - Request removed")
            if(request.accepted){
                render (status: OK, text: [
                        "gameId": request.gameId
                ] as JSON)
            }else{
                render (status: NOT_FOUND)
            }
        }else{
            render (status: OK, text: [
                    "gameId": request.gameId
            ] as JSON)
        }
    }

    def acceptRequest(long id){
        User user = User.findById(session.userId as long)
        GameRequest request = GameRequest.findAndAcceptRequest({ GameRequest req ->
            return req.requestId == id && req.requesterUserId != user.id && req.isInRange(user)
        }, { GameRequest req ->
            gameService.createNewGame(req.configuration, user.id, req.requesterUserId)
        })
        if(request != null){
            render (status: OK, text: [
                    "gameId": request.gameId
            ] as JSON)
        }else{
            render (status: NOT_FOUND)
        }
    }

    def getLobby(){
        List<GameRequest> requests
        if(session.logged){
            User user = User.findById(session.userId as long)
            requests = GameRequest.requests.findAll { it.isInRange(user) }
        }else{
            requests = GameRequest.requests
        }
        render (status: OK, text: [
                "requests": requests*.map
        ] as JSON)
    }

    def getGameInfo(long id, long moves, long timeout){
        Game game = Game.findById(id)
        if(game != null){
            if(session.logged){
                long userId = session.userId as long
                SpectatingUsers.noteAsSpectating(userId, id)
            }
            if(game.isActive){
                gameService.checkForTimeout(game)
            }
            if(game.moveCount == moves){
                if(timeout > 0){
                    final Object lock = new Object()
                    def listener = new GameUpdates.GameListener(){
                        @Override
                        void onUpdate(Game newGame) {
                            if(game.id == newGame.id){
                                // Game is now in the wrong hibernate session and cannot be saved anymore
                                // However the data is the freshest
                                game = newGame
                                synchronized (lock){
                                    lock.notifyAll()
                                }
                            }
                        }
                    }
                    GameUpdates.register(listener)
                    if(game.moveCount == moves){ // Check that the game hasn't changed meanwhile
                        try{
                            synchronized (lock){
                                lock.wait(timeout)
                            }
                        }catch(InterruptedException ignored){
                        }
                    }
                    GameUpdates.unregister(listener)
                }
            }
            render (status: OK, text: game.toMap() as JSON)
        }else{
            render (status: NOT_FOUND)
        }
    }

    static Summary moveLatency = Summary.build("flang_move_latency", "Time to execute a move").register()
    def executeMove(long id, String moveStr){
        Summary.Timer timer = moveLatency.startTimer()
        User user = User.findById(session.userId as long)
        Game game = Game.findByIsActiveAndId(true, id) // Select by isActive due to performance index
        if(game != null){
            if(gameService.checkForTimeout(game)){
                render (status: FORBIDDEN, text: 'Game is not active anymore')
                timer.observeDuration()
                return
            }
            Board board = game.toBoard()
            boolean isWhite = game.white.id == user.id
            boolean isBlack = game.black.id == user.id
            try{
                Move move = MoveParser.parseMove(board, moveStr)
                if(!move.piece.possibleTargetLocations.contains(move.target)){
                    // Invalid move
                    Log.i("GameController", "Invalid move")
                    render (status: FORBIDDEN, text: 'Invalid move')
                    timer.observeDuration()
                    return
                }
                if(!board.gameIsComplete() &&
                        (board.atMove == Color.WHITE && isWhite && move.piece.color == Color.WHITE ||
                        board.atMove == Color.BLACK && isBlack  && move.piece.color == Color.BLACK)){
                    gameService.executeAction(game, move, isWhite ? Color.WHITE : Color.BLACK)
                    render (status: NO_CONTENT)
                }else{
                    Log.i("GameController", "Wrong color trying to move")
                    render (status: FORBIDDEN, text: 'Not your piece')
                }
            }catch(Exception ignored){
                ignored.printStackTrace()
                render (status: FORBIDDEN, text: 'Forbidden')
            }
        }else{
            render (status: NOT_FOUND, text: 'Game not found')
        }
        timer.observeDuration()
    }

    def resign(long id){
        User user = User.findById(session.userId as long)
        Game game = Game.findById(id)
        if(game != null){
            if(!game.isActive){
                render (status: FORBIDDEN)
                return
            }
            if(gameService.checkForTimeout(game)){
                render (status: FORBIDDEN)
                return
            }
            boolean isWhite = game.white == user
            boolean isBlack = game.black == user
            if(isWhite || isBlack){
                Color color = isWhite ? Color.WHITE : Color.BLACK
                gameService.executeAction(game, new Resign(color), color)
                render (status: NO_CONTENT)
            }else {
                render (status: FORBIDDEN)
            }
        }else{
            render (status: NOT_FOUND)
        }
    }

    def findActiveGame(){
        User user = User.findById(session.userId as long)
        List<Game> games = []
        games.addAll(Game.findAllByIsActiveAndWhite(true, user))
        games.addAll(Game.findAllByIsActiveAndBlack(true, user))
        return render (status: OK, text: [
                "games": games.collect { it.toMap() },
        ] as JSON)
    }

    def findComputerResults(String fmn){
        try{
            Board board = new ActionList().addMoveList(fmn).board
            List<ComputerResult> entries = ComputerResult.findAllByFbnAndIsWhite(board.FBN, board.atMove == Color.WHITE)
            if(board.moveNumber < 5){
                FlangComputerInfo info = FlangComputerInfo.findByHost("internal")
                if(info != null){
                    def bot = new FlangComputer(info)
                    bot.getResult(board, bot.node.defaultDepth)
                }
                entries = ComputerResult.findAllByFbnAndIsWhite(board.FBN, board.atMove == Color.WHITE)
            }
            return render (status: OK, text: [
                    "results": entries.collect {["result": it.result, "name": it.algorithm, "depth": it.depth]},
            ] as JSON)
        }catch(Exception e){
            e.printStackTrace()
            return render (status: OK, text: [
                    "results": [],
            ] as JSON)
        }
    }

}
