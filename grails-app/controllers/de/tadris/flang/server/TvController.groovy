package de.tadris.flang.server

import grails.converters.JSON

import static org.springframework.http.HttpStatus.OK

class TvController {

    static long lastTvGame = -1

    def index() {
        Game game = Game.findByIsActiveAndId(true, lastTvGame)
        if(game == null){
            game = Game.find("from Game as game " +
                    "where is_active = true and start_time < 350000 " + // time < 5.5 min
                    "order by greatest(white_rating, black_rating) desc ", [], [max: 1])
        }
        if(game == null){
            game = Game.find("from Game as game " +
                    "where is_active = true and start_time < 1800000 " + // time < 30 min
                    "order by greatest(white_rating, black_rating) desc ", [], [max: 1])
        }
        if(game == null){
            game = Game.findById(lastTvGame)
        }
        lastTvGame = game != null ? game.id : -1
        return render (status: OK, text: [
                "gameId": lastTvGame
        ] as JSON)
    }
}
