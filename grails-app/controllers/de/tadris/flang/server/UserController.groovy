package de.tadris.flang.server

import grails.converters.JSON

import java.util.concurrent.TimeUnit

import static org.springframework.http.HttpStatus.NOT_FOUND
import static org.springframework.http.HttpStatus.OK

class UserController {

    def getInfo(String username) {
        User user = User.findByUsername(username)
        if(user == null){
            render (status: NOT_FOUND)
            return
        }
        List<Map> ratings = UserRatingEntry.findAllByUser(user).collect { it.toMap() }
        render (status: OK, text: [
                "username": user.username,
                "online": OnlineUsers.checkOnlineStatus(user.id),
                "registration": user.registeredAt.time,
                "rating": user.displayedRating,
                "isBot": user.isBot,
                "completedGames": user.completedGames,
                "history": ratings,
                "title": user.title,
        ] as JSON)
    }

    def getGames(String username, int max, int offset){
        max = Math.min(30, max)
        User user = User.findByUsername(username)
        if(user == null){
            render (status: NOT_FOUND)
            return
        }
        def games = Game.findAll("from Game as g where g.white=:user or g.black=:user order by g.id desc",
                [user: user], [max: max, offset: offset]).collect { it.toMap() }
        render (status: OK, text: [
                "games": games,
        ] as JSON)
    }

    def search(String username){
        def users = User.findAllByUsernameLike("%$username%", [max: 10])
        render (status: OK, text: [
                "users": getUserInfos(users)
        ] as JSON)
    }

    def getTopPlayers(){
        Date leastDate = new Date(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(360))
        def users = User.findAll("from User as user " +
                "where user.completedGames >= 15 " +
                "and user.isBot = false " +
                "and (select max(lastMove) from Game as game where black = user or white = user order by id desc) > :leastDate " +
                "order by user.rating desc", [leastDate: leastDate], [max: 15])
        render (status: OK, text: [
                "users": getUserInfos(users)
        ] as JSON)
    }

    def getOnlinePlayers(){
        def users = OnlineUsers.getOnlineUserIds(15).collect { User.findById(it) }
        render (status: OK, text: [
                "users": getUserInfos(users)
        ] as JSON)
    }

    private static List<Map> getUserInfos(List<User> users){
        return users.collect { [
                username: it.username,
                rating: it.displayedRating,
                isBot: it.isBot,
                title: it.title
        ] }
    }

}