package de.tadris.flang.server

import grails.converters.JSON

import java.util.concurrent.TimeUnit

import static org.springframework.http.HttpStatus.OK

class StatsController {

    private static final long INTERVAL = TimeUnit.DAYS.toMillis(180)

    def getStats() {
        render (status: OK, text: [
                "stats" : DayStats.findAllByDateGreaterThan(new Date(System.currentTimeMillis() - INTERVAL))
        ] as JSON)
    }

}
