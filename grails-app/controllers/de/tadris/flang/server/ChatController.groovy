package de.tadris.flang.server

import de.tadris.flang.server.chat.ChatUpdates
import grails.converters.JSON

import java.nio.charset.StandardCharsets
import java.util.concurrent.TimeUnit

import static org.springframework.http.HttpStatus.*

class ChatController {

    private static long CHAT_INTERVAL = TimeUnit.DAYS.toMillis(30)

    def chatService

    def getMessages(long lastMessageDate, long timeout){
        timeout = Math.min(30000, Math.max(5000, timeout))
        lastMessageDate = Math.max(lastMessageDate, System.currentTimeMillis() - CHAT_INTERVAL)
        List<Message> newMessages = Message.findAllBySentAtGreaterThan(new Date(lastMessageDate))
        if(newMessages.size() == 0){
            final Object lock = new Object()
            def listener = new ChatUpdates.ChatListener(){
                @Override
                void onUpdate(Message message) {
                    newMessages = [message]
                    synchronized (lock){
                        lock.notifyAll()
                    }
                }
            }
            ChatUpdates.register(listener)
            try{
                synchronized (lock){
                    lock.wait(timeout)
                }
            }catch(InterruptedException ignored){
            }
            ChatUpdates.unregister(listener)
        }
        return render (status: OK, text: [
                "messages": newMessages.collect { it.toMap() },
        ] as JSON)
    }

    def sendMessage(String text, String attachedGame){
        text = new String(text.decodeBase64Url(), StandardCharsets.UTF_8)
        if(attachedGame == null)
            attachedGame = ""
        attachedGame = new String(attachedGame.decodeBase64Url(), StandardCharsets.UTF_8)

        if((text.trim().isEmpty() && attachedGame.trim().isEmpty()) || text.size() > 1024 || attachedGame?.size() > 2000){
            render (status: FORBIDDEN, text: 'Message empty or too long')
            return
        }
        User user = User.findById(session.userId as long)
        chatService.sendMessage(user, text.trim(), attachedGame.trim())
        render (status: NO_CONTENT)
    }

}
