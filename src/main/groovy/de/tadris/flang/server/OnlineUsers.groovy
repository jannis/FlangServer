package de.tadris.flang.server

import java.util.concurrent.TimeUnit

class OnlineUsers {

    private static final long ONLINE_THRESHOLD = TimeUnit.MINUTES.toMillis(5)
    private static final Object LOCK = new Object()

    private static Map<Long, Long> onlineUsers = [:]

    static void noteAsActive(long userId){
        synchronized (LOCK){
            onlineUsers[userId] = System.currentTimeMillis()
        }
    }

    static boolean checkOnlineStatus(long userId){
        synchronized (LOCK){
            return System.currentTimeMillis() - onlineUsers.getOrDefault(userId, 0) < ONLINE_THRESHOLD
        }
    }

    static int getOnlineUserCount(){
        synchronized (LOCK){
            long time = System.currentTimeMillis() - ONLINE_THRESHOLD
            onlineUsers.removeAll { it.value < time }
            return onlineUsers.size()
        }
    }

    static List<Long> getOnlineUserIds(int max){
        synchronized (LOCK){
            return onlineUsers.keySet().toList().subList(0, Math.min(onlineUsers.size(), max))
        }
    }

}
