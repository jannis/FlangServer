package de.tadris.flang.server.chat

import de.tadris.flang.server.Game
import de.tadris.flang.server.Message

class ChatUpdates {

    private static final Object LOCK = new Object()
    static List<ChatListener> listeners = []

    static void register(ChatListener listener){
        synchronized (LOCK){
            listeners.add(listener)
        }
    }

    static void unregister(ChatListener listener){
        synchronized (LOCK){
            listeners.remove(listener)
        }
    }

    static void onUpdate(Message message){
        synchronized (LOCK){
            listeners.each {
                it.onUpdate(message)
            }
        }
    }

    interface ChatListener {
        void onUpdate(Message message)
    }

}
