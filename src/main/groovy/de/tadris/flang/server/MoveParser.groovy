package de.tadris.flang.server

import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Location
import de.tadris.flang_lib.Piece
import de.tadris.flang_lib.Vector
import de.tadris.flang_lib.action.Move

class MoveParser {

    static Move parseMove(Board board, String str){
        Location from = parseVector(str.substring(1, 3)).toLocation(board)
        Location to = parseVector(str.substring(4, 6)).toLocation(board)
        if(!from.isValid()){
            throw new IllegalArgumentException("Location $from is invalid")
        }
        if(!to.isValid()){
            throw new IllegalArgumentException("Location $to is invalid")
        }
        Piece piece = from.piece.value
        if(!piece){
            throw new IllegalArgumentException("Cannot move null piece at $from on board '$board'")
        }
        return new Move(piece, to)
    }

    private static Vector parseVector(String str){
        assert(str.length() == 2)
        int x = (str.charAt(0) as int) - ('A'.charAt(0) as int)
        int y = str.charAt(1).toString().toInteger() - 1
        return new Vector(x, y)
    }

}
