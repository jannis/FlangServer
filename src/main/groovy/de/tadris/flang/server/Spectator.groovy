package de.tadris.flang.server

class Spectator {

    long userId
    long timestamp

    Spectator(long userId) {
        this.userId = userId
        update()
    }

    void update(){
        timestamp = System.currentTimeMillis()
    }
}
