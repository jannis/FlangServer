package de.tadris.flang.server

class SpectatingUsers {

    private static final long SPECTATING_THRESHOLD = 30_000
    private static final Object LOCK = new Object()

    private static Map<Long, List<Spectator>> spectators = [:] // gameId -> List of spectators

    static void noteAsSpectating(long userId, long gameId){
        synchronized (LOCK){
            def list = getSpectatorListFor(gameId)
            Spectator spectator = list.find { it.userId == userId }
            if(spectator == null){
                spectator = new Spectator(userId)
                list.add(spectator)
            }
            spectator.update()
        }
    }

    static void clearOldSpectators(){
        synchronized (LOCK){
            new ArrayList<Long>(spectators.keySet()).each { long gameId ->
                // getting the spectator count for every game will clear out old spectators
                getSpectatorCount(gameId)
            }
        }
    }

    static int getSpectatorCount(long gameId){
        synchronized (LOCK){
            long time = System.currentTimeMillis() - SPECTATING_THRESHOLD
            def list = spectators.get(gameId)
            if(list == null){
                return 0
            }else{
                list.removeAll { it.timestamp < time }
                if(list.isEmpty()){
                    spectators.remove(gameId)
                }
                return list.size()
            }
        }
    }

    static int getTotalSpectatorCount(){
        synchronized (LOCK){
            int count = 0
            spectators.entrySet().each {
                it?.each { count++ }
            }
            return count
        }
    }

    private static List<Spectator> getSpectatorListFor(long gameId){
        List<Spectator> list = spectators.get(gameId)
        if(list == null){
            list = []
            spectators[gameId] = list
        }
        return list
    }


}
