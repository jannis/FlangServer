package de.tadris.flang.server

import java.util.concurrent.TimeUnit

class RatingBonus {

    static float getCurrentBonus(boolean hasWon){
        Date someDaysAgo = new Date(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(3))
        DayStats entry = DayStats.findByDateGreaterThan(someDaysAgo)
        if(entry == null) return 0f
        int averageUserRating = entry.avgRating
        int difference = User.STANDARD_RATING - averageUserRating
        if(difference.abs() < 10){
            return 0f // not enough difference -> no effect
        }else if(difference > 0 && hasWon){
            return 1f
        }else if(difference < 0 && !hasWon) {
            return -0.5f
        }else{
            return 0f
        }
    }

}
