package de.tadris.flang.server.game

import de.tadris.flang.server.Game

class GameUpdates {

    private static final Object LOCK = new Object()
    static List<GameListener> listeners = []

    static void register(GameListener listener){
        synchronized (LOCK){
            listeners.add(listener)
        }
    }

    static void unregister(GameListener listener){
        synchronized (LOCK){
            listeners.remove(listener)
        }
    }

    static void onUpdate(Game game){
        synchronized (LOCK){
            listeners.each {
                it.onUpdate(game)
            }
        }
    }

    interface GameListener {
        void onUpdate(Game game)
    }

}
