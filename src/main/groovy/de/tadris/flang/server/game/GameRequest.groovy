package de.tadris.flang.server.game

import de.tadris.flang.server.Game
import de.tadris.flang.server.User
import org.jetbrains.annotations.Nullable

class GameRequest {

    private static final Object lock = new Object()
    private static List<RequestListener> listeners = []
    private static List<GameRequest> requests = []
    private static long REQUEST_ID_COUNT = 0

    static void insertRequest(GameRequest request){
        synchronized (lock){
            requests.add(request)
        }
    }

    static void removeRequest(GameRequest request){
        synchronized (lock){
            requests.remove(request)
        }
    }

    static void register(RequestListener listener){
        synchronized (lock){
            listeners.add(listener)
        }
    }

    static void unregister(RequestListener listener){
        synchronized (lock){
            listeners.remove(listener)
        }
    }

    static List<GameRequest> getRequests(){
        synchronized (lock){
            return new ArrayList<GameRequest>(requests)
        }
    }

    @Nullable
    static GameRequest findAndAcceptRequest(Closure<Boolean> filter, Closure<Game> gameCreator){
        synchronized (lock){
            GameRequest request = requests.find { filter(it) }
            if(request != null){
                requests.remove(request)
                request.accept(gameCreator(request))
                listeners.each {
                    it.onUpdate(request)
                }
            }
            return request
        }
    }

    long requestId
    GameConfiguration configuration
    long requesterUserId
    boolean accepted = false
    long gameId
    float rating
    float ratingRange

    GameRequest(GameConfiguration configuration, float ratingRange, User user){
        this.requestId = REQUEST_ID_COUNT++
        this.configuration = configuration
        this.requesterUserId = user.id
        this.rating = user.rating
        this.ratingRange = ratingRange
    }

    void accept(Game game){
        accepted = true
        gameId = game.id
    }

    Map getMap(){
        User requester = User.findById(requesterUserId)
        return [
                id: requestId,
                configuration: [
                        isRated: configuration.isRated,
                        infiniteTime: configuration.infiniteTime,
                        time: configuration.time,
                ],
                requester: [
                        "username": requester.username,
                        "title": requester.title,
                        "isBot": requester.isBot,
                        "rating": requester.displayedRating,
                ]
        ]
    }

    boolean isInRange(User user){
        return ratingRange == -1 || (rating - user.rating).abs() < ratingRange
    }

    interface RequestListener {
        void onUpdate(GameRequest request)
    }

}
