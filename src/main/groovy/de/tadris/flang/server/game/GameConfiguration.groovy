package de.tadris.flang.server.game

class GameConfiguration {

    boolean isRated
    boolean infiniteTime
    long time

    GameConfiguration(boolean isRated, boolean infiniteTime, long time) {
        this.isRated = isRated
        this.infiniteTime = infiniteTime
        this.time = time
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (!(o instanceof GameConfiguration)) return false

        GameConfiguration that = (GameConfiguration) o

        if (infiniteTime != that.infiniteTime) return false
        if (isRated != that.isRated) return false
        if (time != that.time) return false

        return true
    }

    int hashCode() {
        int result
        result = (isRated ? 1 : 0)
        result = 31 * result + (infiniteTime ? 1 : 0)
        result = 31 * result + (int) (time ^ (time >>> 32))
        return result
    }
}
