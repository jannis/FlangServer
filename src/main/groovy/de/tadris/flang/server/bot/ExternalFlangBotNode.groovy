package de.tadris.flang.server.bot

import de.tadris.flang.server.main.GClient
import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Color

class ExternalFlangBotNode implements FlangBotNode {

    String host
    int port

    ExternalFlangBotNode(String host, int port) {
        this.host = host
        this.port = port
    }

    @Override
    String getResult(Board board, int depth, String algorithm){
        boolean isWhite = board.atMove == Color.WHITE
        GClient client = new GClient(host, port)
        String result = client.send("calculate\n" +
                "$board\n" +
                "$isWhite\n" +
                "$depth\n" +
                "$algorithm").trim()
        client.close()
        return result
    }

    @Override
    int getDefaultDepth() {
        return 5
    }

}
