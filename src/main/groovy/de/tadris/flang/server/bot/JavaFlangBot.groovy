package de.tadris.flang.server.bot

import de.tadris.flang_lib.Board
import de.tadris.flang_lib.bot.*
import kotlin.jvm.functions.Function0

class JavaFlangBot implements FlangBotNode {

    @Override
    String getResult(Board board, int depth, String algorithm) {
        def factory = new Function0<BoardEvaluation>() {
            @Override
            BoardEvaluation invoke() {
                return getEvaluation(algorithm)
            }
        }
        int cpus = Runtime.getRuntime().availableProcessors()
        ParallelFlangBot flangBot = new ParallelFlangBot(depth, depth, factory, cpus)
        List<String> list = []
        flangBot.findBestMove(board, true).evaluations.each {
            list.add(it.move.notation + "->" + it.evaluation)
        }
        return list.join(";")
    }

    private static BoardEvaluation getEvaluation(String algorithm){
        switch (algorithm){
            case "BLANK": return new BlankBoardEvaluation(new Board())
            case "STAGE": return new StageEvaluation(new Board())
            default: return new NeoBoardEvaluation()
        }
    }

    @Override
    int getDefaultDepth() {
        return 5
    }
}
