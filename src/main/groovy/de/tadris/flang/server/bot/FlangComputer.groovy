package de.tadris.flang.server.bot

import de.tadris.flang.server.*
import de.tadris.flang_lib.Board
import de.tadris.flang_lib.action.Move

class FlangComputer {

    private AuthService authService = new AuthService()

    FlangComputerInfo info
    String sessionKey

    FlangComputer(FlangComputerInfo info) {
        this.info = info
        User user = User.findByUsername(info.username)
        if(user == null){
            user = new User()
            user.username = info.username
            user.pwdHash = info.loginKey
            authService.createUser(user)
        }
        UserSession session = authService.createSession(user, true)
        sessionKey = session.sessionKey
    }

    void requestGame(Game game){
        new Thread(new FlangComputerGame(this, sessionKey, game.id), "Game#$game.id").start()
    }

    String getResult(Board board, int depth){
        return new StringBotTransformer(node).findResult(board, depth, this.info.algorithm)
    }

    Move findBestMove(Board board, int depth){
        return new StringBotTransformer(node).findBestMove(board, depth, this.info.algorithm)
    }

    FlangBotNode getNode(){
        if(info.host == "internal"){
            return new JavaFlangBot()
        }else{
            return new ExternalFlangBotNode(info.host, info.port)
        }
    }

}
