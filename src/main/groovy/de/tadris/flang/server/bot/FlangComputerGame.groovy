package de.tadris.flang.server.bot

import de.tadris.flang.network_api.FlangAPI
import de.tadris.flang.network_api.model.GameInfo
import de.tadris.flang.network_api.model.GamePlayerInfo
import de.tadris.flang.server.FlangComputerInfo
import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Color
import de.tadris.flang_lib.action.Move
import grails.util.Environment

class FlangComputerGame implements Runnable {

    FlangComputer computer
    FlangComputerInfo info
    long gameId
    FlangAPI api

    FlangComputerGame(FlangComputer computer, String sessionKey, long gameId) {
        this.computer = computer
        this.info = computer.info
        this.gameId = gameId
        if(Environment.current == Environment.DEVELOPMENT){
            api = new FlangAPI("localhost", 8080, "", false, true)
        }else{
            api = new FlangAPI("home.tadris.de", 9090, "flang", true, false)
        }
        api.login(info.username, sessionKey)
    }

    @Override
    void run() {
        int moves = -1
        boolean gameRunning = true
        while (gameRunning){
            GameInfo info = api.getGameInfo(gameId, moves, 5000)
            gameRunning = info.running
            moves = info.moves
            Color myColor = info.white.username == this.info.username ? Color.WHITE : Color.BLACK
            GamePlayerInfo myInfo = myColor == Color.WHITE ? info.white : info.black
            Board board = info.toBoard(new Board())
            if(info.running && board.atMove == myColor){
                // My move
                int depth = this.info.depth
                if(myInfo.time < 30_000){
                    depth--
                }
                if(myInfo.time < 7_000){
                    depth--
                }
                if(myInfo.time > 60_000 && board.findAllMoves(board.atMove).size() < 20){
                    depth++
                }else if(myInfo.time > 60_000 * 3){
                    depth++
                }
                if(myInfo.time > 120_000){
                    sleep((1000 + 5000 * Math.random()) as int)
                }
                try{
                    Move bestMove = computer.findBestMove(board, depth)
                    api.executeMove(gameId, bestMove)
                }catch(Exception e){
                    e.printStackTrace()
                    sleep(10000)
                }
            }
        }
    }


}
