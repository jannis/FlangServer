package de.tadris.flang.server.bot

import de.tadris.flang_lib.Board
import de.tadris.flang_lib.action.Move

interface FlangBotNode {

    String getResult(Board board, int depth, String algorithm)

    int getDefaultDepth()

}
