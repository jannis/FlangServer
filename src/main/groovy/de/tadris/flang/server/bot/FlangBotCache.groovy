package de.tadris.flang.server.bot

import de.tadris.flang.server.ComputerResult
import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Color

class FlangBotCache {

    static synchronized String query(Board board, int minDepth, String algorithm){
        ComputerResult result = null
        ComputerResult.withTransaction {
            result = ComputerResult.findByFbnAndIsWhiteAndDepthGreaterThanEqualsAndAlgorithm(board.FBN, board.atMove == Color.WHITE, minDepth, algorithm)
            if(result != null){
                result.usedCount++
                result.save()
            }
        }
        return result?.result
    }

    static synchronized void saveResult(Board board, int depth, String algorithm, String resultStr){
        ComputerResult.withTransaction {
            ComputerResult oldResult = ComputerResult.findByFbnAndIsWhiteAndDepthLessThanAndAlgorithm(board.FBN, board.atMove == Color.WHITE, depth, algorithm)
            oldResult?.delete() // Delete results (if available) for the same board but less depth

            ComputerResult result = new ComputerResult()
            result.fbn = board.FBN
            result.isWhite = board.atMove == Color.WHITE
            result.depth = depth
            result.createdAt = new Date()
            result.result = resultStr
            result.algorithm = algorithm
            result.insert()
        }
    }

}
