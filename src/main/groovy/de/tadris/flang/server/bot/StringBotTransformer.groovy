package de.tadris.flang.server.bot

import de.tadris.flang.server.MoveParser
import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Color
import de.tadris.flang_lib.action.Move
import de.tadris.flang_lib.bot.MoveEvaluation

class StringBotTransformer {

    private final FlangBotNode node

    StringBotTransformer(FlangBotNode node) {
        this.node = node
    }

    String findResult(Board board, int depth, String algorithm){
        String result = FlangBotCache.query(board, depth, algorithm)
        if(result == null){
            result = node.getResult(board, depth, algorithm)
            FlangBotCache.saveResult(board, depth, algorithm, result)
        }
        return result
    }

    // The higher, the more variance in the first few moves
    static final double VARIANCE = 0.3

    Move findBestMove(Board board, int depth, String algorithm){
        String result = findResult(board, depth, algorithm)

        List<MoveEvaluation> moves = []
        result.split(";").each {
            if(it.empty) return
            String[] moveData = it.split("->")
            Move move = MoveParser.parseMove(board, moveData[0])
            double evaluation = (double)(moveData[1].toFloat()*100 as int) / 100d
            moves.add(new MoveEvaluation(move, evaluation, depth))
        }
        moves = moves.sort { -it.evaluation * board.atMove.evaluationNumber }

        List<MoveEvaluation> goodMoves = moves.findAll {
            if(board.atMove == Color.WHITE){
                return it.evaluation > -100
            }else{
                return it.evaluation < 100
            }
        }
        if(goodMoves.size() > 0){
            double sumEval = goodMoves.sum { it.evaluation } as double
            double avgEval = sumEval / goodMoves.size()
            double variance2 = goodMoves.sum { (it.evaluation - avgEval)**2 } as double
            double variance = Math.sqrt(variance2)
            double maxDiff = variance * VARIANCE / ((board.moveNumber+1) ** 2)
            double bestEval = goodMoves.first().evaluation
            goodMoves = goodMoves.subList(0, Math.min(5, goodMoves.size()))
                    .findAll { (bestEval - it.evaluation).abs() < maxDiff }
            if(goodMoves.size() > 0){
                return goodMoves[Math.random() * goodMoves.size() as int].move
            }
        }
        return moves.first().move
    }

}
