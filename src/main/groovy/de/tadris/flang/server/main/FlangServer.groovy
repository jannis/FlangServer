package de.tadris.flang.server.main

import de.tadris.flang.server.jobs.*

class FlangServer {

    static FlangServer instance

    Scheduler scheduler
    GameCleaner gameCleaner
    SessionCleaner sessionCleaner
    ComputerCleaner computerCleaner
    SpectatorCleaner spectatorCleaner
    AnnouncementCleaner announcementCleaner
    Metrics metrics

    FlangServer() {
        instance = this
        this.scheduler = new Scheduler()
        this.gameCleaner = new GameCleaner()
        this.sessionCleaner = new SessionCleaner()
        this.computerCleaner = new ComputerCleaner()
        this.spectatorCleaner = new SpectatorCleaner()
        this.announcementCleaner = new AnnouncementCleaner()
        StatsCreator.startJob()
        this.metrics = new Metrics()
    }

    void destroy(){
        scheduler.destroy()
        metrics.destroy()
    }
}
