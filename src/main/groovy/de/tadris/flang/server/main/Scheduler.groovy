package de.tadris.flang.server.main

import io.prometheus.client.Counter
import io.prometheus.client.Gauge

import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class Scheduler {

    final Gauge metric_pending_executions = Gauge.build("flang_executions_pending", "Pending executions.").register()
    final Counter metric_execution_count = Counter.build("flang_executions_count", "Executed executions.").register()

    static final long LOOPER_INTERVAL = 30_000

    final synchronized List<Execution> executions= []

    private ExecutorService looperProcessingPool= Executors.newSingleThreadExecutor()
    private boolean isDestroyed = false
    int currentExecutionLevel = Execution.PRIORITY_ALL

    private Thread looperThread

    Scheduler() {
        startLooper()
    }

    void startLooper(){
        looperThread = Thread.start "Looper", {
            Log.d("Looper", "Starting Looper in 10 Seconds")
            sleep(10000)
            while (!isDestroyed){
                try{
                    loop()
                }catch(Exception e){
                    Log.e("Looper", "Loop failed!")
                    e.printStackTrace()
                }
                sleep(LOOPER_INTERVAL)
            }
            Log.w("Looper", "Looper quit")
        }
    }

    void loop(){
        Log.d("Looper", "Looping...")
        long time= new Date().time
        List<Execution> done= []
        synchronized (executions){
            int runCount = 0
            metric_pending_executions.set(executions.size())
            executions.each {
                Execution execution= it
                try{
                    if(time > execution.executionDate.time && execution.priority >= currentExecutionLevel){
                        runCount++
                        looperProcessingPool.execute({
                            try{
                                execution.execute()
                            }catch(Exception e){
                                Log.e("Looper", "Can not execute execution")
                                e.printStackTrace()
                            }
                        })
                        if(execution.removable){
                            // Only remove one-time executions
                            done.add(execution)
                        }
                    }
                } catch(Exception e){
                    Log.e("Looper", "Something went REALLY wrong")
                    e.printStackTrace()
                    if(execution == null){
                        Log.e("Looper", "Execution is NULL! Trying to remove")
                        done.add(execution)
                    }
                }
            }
            metric_execution_count.inc(runCount)
            Log.i("Looper", "Finished looping " + runCount + "/" + executions.size())
            done.each {
                executions.remove(it)
            }
        }
    }

    void destroy(){
        isDestroyed= true
        looperProcessingPool.shutdown()
        looperThread.interrupt()
    }

}
