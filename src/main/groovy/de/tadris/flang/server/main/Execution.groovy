package de.tadris.flang.server.main

class Execution {

    private Closure closure
    Date executionDate
    int priority

    protected Execution(Closure closure, Date executionDate, int priority){
        this.closure= closure
        this.executionDate= executionDate
        this.priority= priority
    }

    void execute(){
        closure()
    }

    void cancel(){
        synchronized (FlangServer.instance.scheduler.executions){
            FlangServer.instance.scheduler.executions.remove(this)
        }
    }

    boolean isRemovable(){
        return true
    }

    static Execution startExecution(Closure closure, long time, int priority= PRIORITY_LOW){
        return startExecution(closure, new Date(new Date().time + time), priority)
    }

    static Execution startExecution(Closure closure, Date executionDate, int priority= PRIORITY_LOW){
        Execution execution= new Execution(closure, executionDate, priority)
        synchronized (FlangServer.instance.scheduler.executions){
            assert execution != null
            FlangServer.instance.scheduler.executions.add(execution)
        }

        return execution
    }

    public static final int PRIORITY_ALL= 0
    public static final int PRIORITY_LOW= 1
    public static final int PRIORITY_HIGH= 2
    public static final int PRIORITY_URGENT= 2

}
