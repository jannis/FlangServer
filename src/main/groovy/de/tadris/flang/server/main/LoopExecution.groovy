package de.tadris.flang.server.main

class LoopExecution extends Execution {

    long interval

    private LoopExecution(Closure closure, long interval, int priority) {
        super(closure, new Date(System.currentTimeMillis()), priority)
        this.interval = interval
    }

    @Override
    void execute() {
        super.execute()
        executionDate = new Date(executionDate.time + interval)
    }

    @Override
    boolean isRemovable() {
        return false
    }

    static LoopExecution startExecution(Closure closure, long interval, int priority= PRIORITY_LOW){
        LoopExecution execution= new LoopExecution(closure, interval, priority)
        synchronized (FlangServer.instance.scheduler.executions){
            assert execution != null
            FlangServer.instance.scheduler.executions.add(execution)
        }

        return execution
    }
}
