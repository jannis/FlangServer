package de.tadris.flang.server.main

import de.tadris.flang.server.ComputerResult
import de.tadris.flang.server.Game
import de.tadris.flang.server.OnlineUsers
import de.tadris.flang.server.SpectatingUsers
import de.tadris.flang.server.User
import io.prometheus.client.Gauge
import io.prometheus.client.exporter.HTTPServer

class Metrics {

    final Gauge activeGames = Gauge.build("flang_games", "Game count").labelNames("state").register()
    final Gauge onlineUsers = Gauge.build("flang_users_online", "Online user count").register()
    final Gauge users = Gauge.build("flang_users", "User count.").register()
    final Gauge botCache = Gauge.build("flang_computer_cache", "Computer cache count.").register()
    final Gauge spectators = Gauge.build("flang_spectators", "Spectator count").register()

    private HTTPServer server

    Metrics(){
        server = new HTTPServer(55606)
        LoopExecution.startExecution({
            Game.withTransaction {
                refreshMetrics()
            }
        }, 60*1000)
    }

    void refreshMetrics(){
        activeGames.labels("active").set(Game.countByIsActive(true))
        activeGames.labels("completed").set(Game.countByIsActive(false))
        botCache.set(ComputerResult.count())
        users.set(User.count())
        onlineUsers.set(OnlineUsers.getOnlineUserCount())
        spectators.set(SpectatingUsers.totalSpectatorCount)
    }

    void destroy(){
        server.stop()
    }

}
