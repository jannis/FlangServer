package de.tadris.flang.server.main

import java.text.SimpleDateFormat

class Log {

    static final int DEBUG= 0
    static final int INFO= 1
    static final int WARNING= 2
    static final int ERROR= 3

    static int MIN_LOGLEVEL= WARNING

    static void e(String tag, String msg){
        log(tag, msg, ERROR)
    }
    static void w(String tag, String msg){
        log(tag, msg, WARNING)
    }
    static void i(String tag, String msg){
        log(tag, msg, INFO)
    }
    static void d(String tag, String msg){
        log(tag, msg, DEBUG)
    }

    static void log(String first, String msg, int level= 0){
        if(level < MIN_LOGLEVEL){
            return
        }
        String loglevel
        switch (level){
            case DEBUG:   loglevel= "[ DEB]"; break
            case WARNING: loglevel= "[WARN]"; break
            case ERROR:   loglevel= "[ ERR]"; break
            default:      loglevel= "[INFO]"; break
        }
        first= "[" + first + "]"
        output "[$date][$loglevel]$first: $msg"
    }

    static void output(String msg){
        println msg
    }

    static String getDate(){
        return new SimpleDateFormat("HH:mm:ss").format(new Date())
    }

}
