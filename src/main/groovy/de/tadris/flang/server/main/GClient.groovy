package de.tadris.flang.server.main

class GClient {
    int serverPort
    String hostName
    InetAddress host
    Socket socket
    PrintWriter toServer
    BufferedReader fromServer

    GClient(String hostname, int port) throws IOException {
        hostName = hostname
        serverPort = port
        host = InetAddress.getByName(hostName)
        socket = new Socket(host, serverPort)
        fromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()))
        toServer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()))
    }

    static void logRequest(String request, String response) {
        log("Sending:  " + request)
        log("Response: " + response)
    }

    static void log(String str) {
        Log.d("GClient", str)
    }

    String send(String request) throws IOException {
        toServer.println(request)
        toServer.flush()
        String response = fromServer.readLine()
        logRequest(request, response)
        return response
    }

    void close() {
        try {
            toServer.close()
            fromServer.close()
            socket.close()
        } catch (IOException e) {
            e.printStackTrace()
        }
    }

}
