package de.tadris.flang.server.main

import java.util.concurrent.TimeUnit;

class BruteforceProtection {

    private static Map<String, Integer> bruteforceDenial = [:]

    static void init(){
        LoopExecution.startExecution({
            bruteforceDenial.clear()
        }, TimeUnit.DAYS.toMillis(2))
    }

    static void countBruteforce(String addr){
        Log.w("DeleteAccount", "$addr performed illegal account deletion")
        if(bruteforceDenial.containsKey(addr)){
            bruteforceDenial[addr] = bruteforceDenial[addr] + 1
        }else{
            bruteforceDenial[addr] = 1
        }
    }

    static boolean isTimeouted(String addr){
        return bruteforceDenial.containsKey(addr) && bruteforceDenial[addr] > 5
    }

}
