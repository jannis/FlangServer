package de.tadris.flang.server

import de.tadris.flang_lib.Board
import de.tadris.flang_lib.Color
import de.tadris.flang_lib.action.ActionList
import de.tadris.flang_lib.action.Move

class OpeningDatabaseBuilder {

    private static final CHUNK_SIZE = 50
    private static final MAX_DEPTH = 10

    static void addAll(){
        int count = Game.count()
        int currentOffset = 0
        while (currentOffset < count){
            println("Building opening database... $currentOffset/$count")
            List<Game> currentChunk = Game.findAll(max: CHUNK_SIZE, offset: currentOffset)
            currentChunk.each {
                if(!it.isActive){
                    addGame(it)
                }
            }
            currentOffset+= CHUNK_SIZE
        }
        println("Built opening database: " + OpeningDatabaseEntry.count() + " entries.")
    }

    static void addGame(Game game){
        addBoard(game.toBoard(), game.won)
    }

    private static void addBoard(Board originalBoard, int won){
        List<Move> moveList = originalBoard.moveList.actions.findAll { it instanceof Move } as List<Move>
        int max = Math.min(MAX_DEPTH, moveList.size()) - 1
        for(int moves = 0; moves <= max; moves++){
            def actionList = new ActionList()
            actionList.actions.addAll(moveList.subList(0, moves))
            Board boardWithMoves = actionList.regenerateBoard()
            Move nextMove = moveList[moves]
            addMove(boardWithMoves, nextMove, won > 0)
        }
    }

    private static void addMove(Board board, Move move, boolean whiteHasWon){
        String fbn = board.FBN
        boolean isWhite = move.piece.color == Color.WHITE
        String moveNotation = move.notation
        OpeningDatabaseEntry entry = OpeningDatabaseEntry.findByFbnAndIsWhiteAndMove(fbn, isWhite, moveNotation)
        if(entry == null){
            entry = new OpeningDatabaseEntry()
            entry.fbn = fbn
            entry.isWhite = isWhite
            entry.move = moveNotation
        }
        entry.gameCount++
        if(isWhite ^ whiteHasWon){
            // other color has won
            entry.looseCount++
        }else{
            // my color has won
            entry.winCount++
        }
        entry.save()
    }

}
