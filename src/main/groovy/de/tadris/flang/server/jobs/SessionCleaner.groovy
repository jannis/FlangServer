package de.tadris.flang.server.jobs

import de.tadris.flang.server.UserSession
import de.tadris.flang.server.main.LoopExecution

import java.util.concurrent.TimeUnit

class SessionCleaner {

    static final long SESSION_TIMEOUT_DEFAULT = TimeUnit.DAYS.toMillis(30)
    static final long SESSION_TIMEOUT_TEMPORARY = TimeUnit.HOURS.toMillis(1)

    SessionCleaner() {
        LoopExecution.startExecution({
            UserSession.withTransaction {
                cleanAll()
            }
        }, TimeUnit.DAYS.toMillis(1))
    }

    static void cleanAll(){
        Date timeoutDefaultSessions = new Date(System.currentTimeMillis() - SESSION_TIMEOUT_DEFAULT)
        UserSession.findAllByTemporaryAndLastLoginLessThan(false, timeoutDefaultSessions).each {
            clearSession(it)
        }
        Date timeoutTempSessions = new Date(System.currentTimeMillis() - SESSION_TIMEOUT_TEMPORARY)
        UserSession.findAllByTemporaryAndLastLoginLessThan(true, timeoutTempSessions).each {
            clearSession(it)
        }
    }

    static void clearSession(UserSession session){
        session.delete()
    }

}
