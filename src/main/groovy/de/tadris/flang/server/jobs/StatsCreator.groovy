package de.tadris.flang.server.jobs

import de.tadris.flang.server.DayStats
import de.tadris.flang.server.DayStatsService
import de.tadris.flang.server.main.LoopExecution

import java.util.concurrent.TimeUnit

class StatsCreator {

    static void startJob(){
        LoopExecution.startExecution({
            DayStats.withTransaction {
                new DayStatsService().collectStats()
            }
        }, TimeUnit.DAYS.toMillis(1))
    }

}
