package de.tadris.flang.server.jobs

import de.tadris.flang.server.SpectatingUsers
import de.tadris.flang.server.main.LoopExecution

import java.util.concurrent.TimeUnit

class SpectatorCleaner {

    SpectatorCleaner() {
        LoopExecution.startExecution({
            clean()
        }, TimeUnit.MINUTES.toMillis(10))
    }

    static void clean(){
        SpectatingUsers.clearOldSpectators()
    }

}
