package de.tadris.flang.server.jobs

import de.tadris.flang.server.ServerAnnouncement
import de.tadris.flang.server.main.LoopExecution

import java.util.concurrent.TimeUnit

class AnnouncementCleaner {

    AnnouncementCleaner() {
        LoopExecution.startExecution({
            ServerAnnouncement.withTransaction {
                cleanAll()
            }
        }, TimeUnit.DAYS.toMillis(1))
    }

    private static void cleanAll(){
        ServerAnnouncement.findAllByShowUntilLessThan(new Date()).each {
            it.delete()
        }
    }

}
