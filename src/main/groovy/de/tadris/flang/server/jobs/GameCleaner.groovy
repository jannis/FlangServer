package de.tadris.flang.server.jobs

import de.tadris.flang.server.Game
import de.tadris.flang.server.GameService
import de.tadris.flang.server.main.LoopExecution

import java.util.concurrent.TimeUnit

class GameCleaner {

    private GameService gameService = new GameService()

    GameCleaner() {
        LoopExecution.startExecution({
            Game.withTransaction {
                cleanAll()
            }
        }, TimeUnit.HOURS.toMillis(1))
    }

    void cleanAll(){
        Game.findAllByIsActive(true).each {
            checkGame(it)
        }
    }

    void checkGame(Game game){
        gameService.checkForTimeout(game)
    }

}
