package de.tadris.flang.server.jobs

import de.tadris.flang.server.ComputerResult
import de.tadris.flang.server.main.LoopExecution

import java.util.concurrent.TimeUnit

class ComputerCleaner {

    static final long CLEANUP_TIME = TimeUnit.DAYS.toMillis(14)

    ComputerCleaner(){
        LoopExecution.startExecution({
            ComputerResult.withTransaction {
                clean()
            }
        }, TimeUnit.DAYS.toMillis(1))
    }

    private static void clean(){
        ComputerResult.findAllByUsedCountAndCreatedAtLessThan(0, new Date(System.currentTimeMillis() - CLEANUP_TIME)).each {
            it.delete()
        }
    }


}
